var express = require('express');
var router = express.Router();
var controller = require('../controllers/books.controller');
var authenticate = require("../middleware/authenticate.middleware")

/* GET books listing. */
router.get(
    '/', 
   // authenticate.isUser,
    //authenticate.isSuperAdmin, //la func n'existe pas mais c pr donner une idée
    controller.getAll
); //on peut enchainer autant de fonction qu'on veut

router.post("/", controller.addOne);
router.get("/:bookId", controller.getOne);
router.put("/:bookId", controller.updateOne);
router.delete("/:bookId", controller.deleteOne);

router.get("/:bookId/comments", controller.getComments);
router.post("/:bookId/comments", controller.addComment);
router.get("/:bookId/comments/:commentId", controller.getComment);
router.put("/:bookId/comments/:commentId", controller.updateComment);
router.delete("/:bookId/comments/:commentId", controller.deleteComment);

module.exports = router;
