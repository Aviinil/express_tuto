const Book = require("../models/book.model");

var controller = {
  getAll: async function (req, res, next) {
    try {
      let books = await Book.find({});
      res.json(books);
    } catch (err) {
      console.error(err);
      next(err);
    }
  },
  addOne: async (req, res, next) => {
    try {
      //mongoose verif le schema
      let book = await Book.create(req.body);
      res.json(book);
    } catch (err) {
      console.error(err);
      next(err);
    }
  },
  getOne: async (req, res, next) => {
    try {
      let book = await Book.findById(req.params.bookId);
      res.json(book);
    } catch (err) {
      console.error(err);
      next(err);
    }
  },
  updateOne: async (req, res, next) => {
    try {
      let book = await Book.findByIdAndUpdate(req.params.bookId, req.body, {
        new: true,
      });
      res.json(book);
    } catch (err) {
      console.error(err);
      next(err);
    }
  },
  deleteOne: async (req, res, next) => {
    try {
      let out = await Book.findByIdAndDelete(req.params.bookId);
      res.json(out);
    } catch (err) {
      console.error(err);
      next(err);
    }
  },
  getComments: async (req, res, next) => {
    try {
      let book = await Book.findById(req.params.bookId);
      res.json(book.comments);
    } catch (err) {
      console.error(err);
      next(err);
    }
  },
  addComment: async (req, res, next) => {
    try {
      // let book = await Book.findByIdAndUpdate(
      //  { _id: req.params.bookId },
      //   { $push: { comments: req.body }, new: true }
      // );
      let book = await Book.findById(req.params.bookId);
      book.comments.push(req.body);
      await book.save(); //on verif que tous les champs required sont remplis
      res.json(book);
    } catch (err) {
      console.error(err);
      next(err);
    }
  },
  getComment: async (req, res, next) => {
    try {
      let book = await Book.findById(req.params.bookId);
      let comment = book.comments.find((el) => el._id == req.params.commentId);
      if (comment == undefined) {
        res.json({});
      } else {
        res.json(comment);
      }
    } catch (err) {
      console.error(err);
      next(err);
    }
  },
  updateComment: async (req, res, next) => {
    try {
      let book = await Book.findById(req.params.bookId);
      //let comment = book.comments.find((el) => el._id == req.params.commentId);
      let commentIndex = book.comments.findIndex(
        (el) => el._id == req.params.commentId
      );
      if (commentIndex == undefined) {
        res.json({});
      } else {
        book.comments[commentIndex] = req.body;
        await book.save();
        res.json(book.comments[commentIndex]);
      }
    } catch (err) {
      console.error(err);
      next(err);
    }
  },
  deleteComment: async (req, res, next) => {
    try {
      let book = await Book.findById(req.params.bookId);
      //let comment = book.comments.find((el) => el._id == req.params.commentId);
      let commentIndex = book.comments.findIndex(
        (el) => el._id == req.params.commentId
      );
      if (commentIndex == undefined) {
        res.json({});
      } else {
        book.comments.splice(commentIndex, 1);
        await book.save();
        res.json(book.comments[commentIndex]);
      }
    } catch (err) {
      console.error(err);
      next(err);
    }
  },
};

module.exports = controller;
