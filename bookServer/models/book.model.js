const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const bookSchema = new Schema(
  {
    title: { type: String, required: true },
    author: { type: String, required: true },
    image: { type: String },
    year: { type: Number },
    price: { type: Number },
    bestseller: { type: Boolean, default: false },
    comments: [
      {
        rating: { type: Number, min: 1, max: 5, required: true },
        comment: { type: String, required: true },
        author: { type: String, required: true },
      },
    ],
  },
  { collection: "books", timestamps: true }
);

module.exports = mongoose.model("Book", bookSchema);
